const HtmlWebpackPlugin = require("html-webpack-plugin")
const path = require("path")

const dist = path.join(__dirname, "/dist")
const src = path.join(__dirname, "/src")

module.exports = {
    context: src,
    devtool: "#cheap-module-eval-source-map",
    entry: "./lib.js",
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.js$/,
                use: "babel-loader"
            }
        ]
    },
    output: {
        filename: "[name].js",
        path: path.join(__dirname, "/public")
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html'
        })
    ],
    target: "web",
    watch: true
}
